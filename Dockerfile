FROM debian:9 as build
RUN echo "deb http://archive.debian.org/debian stretch main contrib non-free" > /etc/apt/sources.list
RUN echo "deb http://archive.debian.org/debian/ stretch-proposed-updates main contrib non-free" >> /etc/apt/sources.list
RUN echo "deb http://archive.debian.org/debian-security stretch/updates main contrib non-free" >> /etc/apt/sources.list

RUN apt update && apt upgrade -y && apt install -y wget gcc make libpcre2-8-0 libpcre3 libpcre3-dev openssl libssl-dev g++ zlib1g zlib1g-dev
RUN wget http://nginx.org/download/nginx-1.25.0.tar.gz && tar xvfz nginx-1.25.0.tar.gz && cd nginx-1.25.0 && ./configure && make -j4 && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log ../logs/access.log && chmod +x nginx && useradd -s /bin/false nginx
COPY ./nginx.conf /usr/local/nginx/conf 
COPY ./index.html /usr/local/nginx
CMD ["./nginx", "-g", "daemon off;" ]
